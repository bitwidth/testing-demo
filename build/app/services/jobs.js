"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var report_jobs_1 = require("../daos/report_jobs");
var reportJobsDAO = new report_jobs_1.ReportJobsDAO();
var JobService = /** @class */ (function () {
    function JobService() {
    }
    JobService.listAllJobs = function () {
        return new Promise(function (resolve, reject) {
            reportJobsDAO.listAll()
                .then(function (allJobs) {
                // console.log('Success. Listed all the jobs.');
                resolve(allJobs);
            })
                .catch(function (error) {
                console.log('Error: ', error);
                reject(error);
            });
        });
    };
    JobService.listAllFailedJobs = function () {
        return new Promise(function (resolve, reject) {
            reportJobsDAO.listFailed()
                .then(function (failedJobs) {
                // console.log('Success. Listed all the failed jobs.');
                resolve(failedJobs);
            })
                .catch(function (error) {
                console.log('Error: ', error);
                reject(error);
            });
        });
    };
    JobService.listOneJob = function (jobId) {
        return new Promise(function (resolve, reject) {
            reportJobsDAO.listOne(jobId)
                .then(function (listJob) {
                // console.log('Success. Listed one job with \'job_id\' = '+jobId);
                resolve(listJob);
            })
                .catch(function (error) {
                console.log('Error: ', error);
                reject(error);
            });
        });
    };
    JobService.addOneJob = function () {
        return new Promise(function (resolve, reject) {
            reportJobsDAO.addOne()
                .then(function (addedJob) {
                // console.log('Success. Added one job.');
                resolve(addedJob);
            })
                .catch(function (error) {
                console.log('Error: ', error);
                reject(error);
            });
        });
    };
    return JobService;
}());
exports.JobService = JobService;
