"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var bodyParser = require("body-parser");
var app = express();
exports.app = app;
var router = express.Router();
var jobs_1 = require("./routes/jobs");
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.get('/', function (request, response) {
    response.status(200);
    response.json({
        error: false,
        data: "server is working fine!"
    });
});
router.get("/jobId/:jobId", jobs_1.JobsAPI.listOneJob);
router.get("/allJobs", jobs_1.JobsAPI.listAllJobs);
router.get("/failedJobs", jobs_1.JobsAPI.listAllFailedJobs);
router.post("/addOne", jobs_1.JobsAPI.addOneJob);
router.get("/*", function (request, response) {
    response.status(404);
    response.json({
        error: true,
        data: "The requested route doesn't exist. :("
    });
});
app.use('/', router);
