"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var jobs_1 = require("../services/jobs");
var JobsAPI = /** @class */ (function () {
    function JobsAPI() {
    }
    JobsAPI.listAllJobs = function (request, response) {
        jobs_1.JobService.listAllJobs()
            .then(function (allJobs) {
            response.status(200);
            response.send({
                error: false,
                data: allJobs
            });
        })
            .catch(function (error) {
            response.status(500);
            response.send({
                error: true,
                data: "Please try again. Couldn't list all jobs."
            });
        });
    };
    JobsAPI.listAllFailedJobs = function (request, response) {
        jobs_1.JobService.listAllFailedJobs()
            .then(function (failedJobs) {
            response.status(200);
            response.send({
                error: false,
                data: failedJobs
            });
        })
            .catch(function (error) {
            response.status(500);
            response.send({
                error: true,
                data: "Please try again. Couldn't list all \'failed\' jobs."
            });
        });
    };
    JobsAPI.listOneJob = function (request, response) {
        var jobId = request.params.jobId;
        jobs_1.JobService.listOneJob(jobId)
            .then(function (oneJob) {
            response.status(200);
            response.send({
                error: false,
                data: oneJob
            });
        })
            .catch(function (error) {
            response.status(500);
            response.send({
                error: true,
                data: "Please try again. Couldn't list all \'failed\' jobs."
            });
        });
    };
    JobsAPI.addOneJob = function (request, response) {
        jobs_1.JobService.addOneJob()
            .then(function (addedJob) {
            response.status(201);
            response.send({
                error: false,
                data: addedJob
            });
        })
            .catch(function (error) {
            response.status(500);
            response.send({
                error: true,
                data: "Please try again. Couldn't add the job." + error
            });
        });
    };
    return JobsAPI;
}());
exports.JobsAPI = JobsAPI;
