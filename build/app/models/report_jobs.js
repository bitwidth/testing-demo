"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var statusDeveloper = ['dequeued', 'generated​', 'uploaded', 'storage', 'notified', 'failed'];
var statusGeneral = ['processing​', 'uploading​', 'completed​', 'failed​'];
var ReportJobsModel = /** @class */ (function () {
    function ReportJobsModel(sequelize, DataTypes) {
        var reportJobs = sequelize.define('report_job', {
            job_id: { type: DataTypes.UUID, primaryKey: true },
            client_request_id: { type: DataTypes.UUID, allowNull: false },
            report_request_id: { type: DataTypes.UUID, allowNull: false },
            status_developer: { type: DataTypes.ENUM, allowNull: false, values: statusDeveloper, defaultValue: statusDeveloper[0] },
            status_general: { type: DataTypes.ENUM, allowNull: false, values: statusGeneral, defaultValue: statusGeneral[0] },
            generated_timestamp: { type: DataTypes.DATE, allowNull: false }
        });
        return reportJobs;
    }
    return ReportJobsModel;
}());
exports.ReportJobsModel = ReportJobsModel;
