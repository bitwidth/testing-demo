"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = require("./connection");
var uuidv4 = require('uuid').v4;
var statusDeveloper = ['dequeued', 'generated​', 'uploaded', 'notified', 'failed'];
var statusGeneral = ['processing​', 'uploading​', 'completed​', 'failed​'];
var ReportJobsDAO = /** @class */ (function () {
    function ReportJobsDAO() {
        var _this = this;
        this.model = connection_1.sequelize.model("report_job");
        this.listAll = function () {
            return _this.model.findAll({
                where: {}
            });
        };
        this.listFailed = function () {
            return _this.model.findAll({
                where: {
                    status_developer: 'failed'
                }
            });
        };
        this.listOne = function (jobId) {
            return _this.model.findAll({
                where: {
                    job_id: jobId
                }
            });
        };
        this.addOne = function () {
            return _this.model.create({
                job_id: uuidv4(),
                client_request_id: uuidv4(),
                report_request_id: uuidv4(),
                status_developer: statusDeveloper[Math.floor((Math.random() * 5))],
                status_general: statusGeneral[Math.floor((Math.random() * 4))],
                generated_timestamp: new Date().toUTCString()
            });
        };
    }
    return ReportJobsDAO;
}());
exports.ReportJobsDAO = ReportJobsDAO;
