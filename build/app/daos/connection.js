"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var models = __importStar(require("../models/allModels"));
var fs = require('fs');
var path = require("path");
var modelsObject = models;
var Sequelize = require('sequelize');
var postgresObj = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../../../config/postgres.json'), 'utf-8'));
var sequelize = new Sequelize(postgresObj.databaseName, postgresObj.userName, postgresObj.password, {
    host: postgresObj.host,
    port: postgresObj.port,
    dialect: 'postgres',
    pool: {
        acquire: 20000,
    },
    logging: false
});
exports.sequelize = sequelize;
/* or with an URI

const sequelize = new Sequelize('postgres://username:password@localhost:port/database_name',{
    options...
});

*/
sequelize.authenticate()
    .then(function (result) {
    // console.log("\nConnection established to database: "+sequelize.getDatabaseName());
})
    .catch(function (error) {
    console.error('\nError - ' + error);
    process.exit(1);
});
Object.keys(models).forEach(function (eachModel) {
    var model = modelsObject[eachModel](sequelize, Sequelize.DataTypes);
    model.sync()
        .then(function (result) {
        // console.log('\nSynchronized table: \''+result.getTableName()+'\' in database.');
    })
        .catch(function (error) {
        console.error('\nSomething went wrong while syncing :' + error);
    });
});
