"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var session = require('supertest-session');
var chai_1 = require("chai");
var app_1 = require("../../app/app");
var testSession = null;
var server;
var jobId;
describe("supertest-session API calls:", function () {
    before(function (done) {
        var PORT = process.env.PORT || 3004;
        server = app_1.app.listen(PORT, function () {
            // console.log('\nListening on port '+PORT);
        });
        testSession = session(server);
        done();
    });
    afterEach(function (done) {
        console.log("\nCookie name:" + testSession.cookies[0].name);
        console.log("Cookie value:" + testSession.cookies[0].value);
        done();
    });
    it('getting a cookie', function (done) {
        testSession
            .get('/')
            .expect(200)
            .expect(function (response) {
            chai_1.expect(response.body.error).false;
            chai_1.expect(response.body.data).exist;
            chai_1.expect(response.headers['set-cookie'].exist);
        })
            .end(done);
    });
    after(function (done) {
        server.close();
        done();
    });
    describe('POST /addOne', function () {
        it('should add one job into the table with status 201', function (done) {
            testSession
                .post('/addOne')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(201)
                .expect(function (response) {
                chai_1.expect(response.body.error).false;
                chai_1.expect(response.body.data).exist;
            })
                .end(done);
        });
    });
    describe('GET /', function () {
        it('should get a response with status 200', function (done) {
            testSession
                .get("/")
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .expect({
                error: false,
                data: 'server is working fine!'
            })
                .end(done);
        });
    });
    describe('GET /allJobs', function () {
        it('should get all jobs in response with status 200', function (done) {
            testSession
                .get("/allJobs")
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .expect(function (response) {
                chai_1.expect(response.body.error).false;
                chai_1.expect(response.body.data).exist;
            })
                .end(done);
        });
    });
    describe('GET /failedJobs', function () {
        it('should get all failed jobs in response with status 200', function (done) {
            testSession
                .get("/failedJobs")
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .expect(function (response) {
                chai_1.expect(response.body.error).false;
                chai_1.expect(response.body.data).exist;
            })
                .end(done);
        });
    });
    describe('GET /jobId/:jobId', function () {
        it('should retrieve the a job by it\'s \'jobId\' in response with status 200', function (done) {
            testSession
                .post('/addOne')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(201)
                .expect(function (response) {
                chai_1.expect(response.body.error).false;
                jobId = (response.body.data.job_id).exist;
            })
                .then(function (res) {
                jobId = res.body.data.job_id;
                testSession
                    .get("/jobId/" + jobId)
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(200)
                    .expect(function (response) {
                    chai_1.expect(response.body.error).false;
                    chai_1.expect(response.body.data).exist;
                    chai_1.expect(response.body.data[0].job_id).equal(jobId);
                })
                    .end(done);
            });
        });
    });
    describe('GET /randomGarbage', function () {
        it('should get response with status 404', function (done) {
            testSession
                .get("/randomGarbage")
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(404)
                .expect(function (response) {
                chai_1.expect(response.body.error).true;
                chai_1.expect(response.body.data).exist;
            })
                .end(done);
        });
    });
});
