"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var supertest = require("supertest");
var chai_1 = require("chai");
var app_1 = require("../../app/app");
var request;
var server;
var jobId;
describe("supertest API calls:", function () {
    before(function (done) {
        var PORT = process.env.PORT || 3004;
        server = app_1.app.listen(PORT, function () {
            // console.log('\nListening on port '+PORT);
        });
        request = supertest(server);
        done();
    });
    after(function (done) {
        server.close();
        done();
    });
    afterEach(function (done) {
        if (request.cookies !== undefined) {
            console.log("\nCookie name:" + request.cookies[0].name);
            console.log("Cookie value:" + request.cookies[0].value);
        }
        done();
    });
    it('getting a cookie', function (done) {
        request
            .get('/')
            .expect(200)
            .expect(function (response) {
            chai_1.expect(response.body.error).false;
            chai_1.expect(response.body.data).exist;
            chai_1.expect(response.headers['set-cookie'].exist);
            chai_1.expect(response.headers['set-cookie'].not.empty);
        })
            .end(done);
    });
    describe('POST /addOne', function () {
        it('should add one job into the table with status 201', function (done) {
            request
                .post('/addOne')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(201)
                .expect(function (response) {
                chai_1.expect(response.body.error).false;
                chai_1.expect(response.body.data).exist;
            })
                .end(done);
        });
    });
    describe('GET /', function () {
        it('should get a response with status 200', function (done) {
            request
                .get("/")
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .expect({
                error: false,
                data: 'server is working fine!'
            })
                .end(done);
        });
    });
    describe('GET /allJobs', function () {
        it('should get all jobs in response with status 200', function (done) {
            request
                .get("/allJobs")
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .expect(function (response) {
                chai_1.expect(response.body.error).false;
                chai_1.expect(response.body.data).exist;
            })
                .end(done);
        });
    });
    describe('GET /failedJobs', function () {
        it('should get all failed jobs in response with status 200', function (done) {
            request
                .get("/failedJobs")
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .expect(function (response) {
                chai_1.expect(response.body.error).false;
                chai_1.expect(response.body.data).exist;
            })
                .end(done);
        });
    });
    describe('GET /jobId/:jobId', function () {
        it('should retrieve the a job by it\'s \'jobId\' in response with status 200', function (done) {
            request
                .post('/addOne')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(201)
                .expect(function (response) {
                chai_1.expect(response.body.error).false;
                jobId = (response.body.data.job_id).exist;
            })
                .then(function (res) {
                jobId = res.body.data.job_id;
                request
                    .get("/jobId/" + jobId)
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(200)
                    .expect(function (response) {
                    chai_1.expect(response.body.error).false;
                    chai_1.expect(response.body.data).exist;
                    chai_1.expect(response.body.data[0].job_id).equal(jobId);
                })
                    .end(done);
            });
        });
    });
    describe('GET /randomGarbage', function () {
        it('should get response with status 404', function (done) {
            request
                .get("/randomGarbage")
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(404)
                .expect(function (response) {
                chai_1.expect(response.body.error).true;
                chai_1.expect(response.body.data).exist;
            })
                .end(done);
        });
    });
});
