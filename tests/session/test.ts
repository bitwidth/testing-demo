var session = require('supertest-session');
import { expect } from "chai";

import { app } from "../../app/app";

let testSession:any = null;
let server:any;
let jobId:any;
describe("supertest-session API calls:", () => {
    
    before(done =>{
        const PORT = process.env.PORT || 3004;

        server = app.listen(PORT, () => {
            // console.log('\nListening on port '+PORT);
        });

        testSession = session(server);
        done();
    });

    afterEach(done =>{
        console.log("\nCookie name:"+testSession.cookies[0].name);
        console.log("Cookie value:"+testSession.cookies[0].value);
        done();
    });

    it('getting a cookie', (done) => {
        testSession
        .get('/')
        .expect(200)
        .expect((response:any) => {
            expect(response.body.error).false;
            expect(response.body.data).exist;
            expect(response.headers['set-cookie'].exist);
        })
        .end(done)
    });


    after(done => {
        server.close();
        done();
    });


    describe('POST /addOne', () => {

        it('should add one job into the table with status 201', (done) => {
            testSession
            .post('/addOne')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .expect( (response:any) => {
                expect(response.body.error).false;
                expect(response.body.data).exist;
            })
            .end(done);
        });
    });


    describe('GET /', () => {
        
        it('should get a response with status 200', (done) => {
            testSession
            .get("/")
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect({
                error: false,
                data: 'server is working fine!'
            })
            .end(done);
        })
    });


    describe('GET /allJobs', () => {
        
        it('should get all jobs in response with status 200', (done) => {
            testSession
            .get("/allJobs")
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect( (response:any) => {
                expect(response.body.error).false;
                expect(response.body.data).exist;
            })
            .end(done);
        })
    });

    
    describe('GET /failedJobs', () => {
        
        it('should get all failed jobs in response with status 200', (done) => {
            testSession
            .get("/failedJobs")
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect( (response:any) => {
                expect(response.body.error).false;
                expect(response.body.data).exist;
            })
            .end(done);
        })
    });

    describe('GET /jobId/:jobId', () => {
        
        it('should retrieve the a job by it\'s \'jobId\' in response with status 200', (done) => {            
            testSession
            .post('/addOne')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .expect( (response:any) => {
                expect(response.body.error).false;
                jobId = (response.body.data.job_id).exist;
            })
            .then((res:any) => {
                jobId = res.body.data.job_id;
                testSession
                .get("/jobId/"+jobId)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .expect( (response:any) => {
                    expect(response.body.error).false;
                    expect(response.body.data).exist;
                    expect(response.body.data[0].job_id).equal(jobId);
                })
                .end(done);
            });
            
        });
    });


    describe('GET /randomGarbage', () => {
        
        it('should get response with status 404', (done) => {
            testSession
            .get("/randomGarbage")
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(404)
            .expect( (response:any) => {
                expect(response.body.error).true;
                expect(response.body.data).exist;
            })
            .end(done);
        })
    });

});
