const supertest = require("supertest");
import { expect } from "chai";

import { app } from "../../app/app";

let request:any;
let server:any;
let jobId:any;
describe("supertest API calls:", () => {
    
    before(done =>{
        const PORT = process.env.PORT || 3004;

        server = app.listen(PORT, () => {
            // console.log('\nListening on port '+PORT);
        });

        request = supertest(server);
        done();
    });

    after(done => {
        server.close();
        done();
    });

    afterEach(done =>{
        if(request.cookies !== undefined) {
            console.log("\nCookie name:"+request.cookies[0].name);
            console.log("Cookie value:"+request.cookies[0].value);
        }

        done();
    });

    it('getting a cookie', (done) => {
        request
        .get('/')
        .expect(200)
        .expect((response:any) => {
            expect(response.body.error).false;
            expect(response.body.data).exist;
            expect(response.headers['set-cookie'].exist);
            expect(response.headers['set-cookie'].not.empty);
        })
        .end(done)
    });

    describe('POST /addOne', () => {

        it('should add one job into the table with status 201', (done) => {
            request
            .post('/addOne')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .expect( (response:any) => {
                expect(response.body.error).false;
                expect(response.body.data).exist;
            })
            .end(done);
        });
    });


    describe('GET /', () => {
        
        it('should get a response with status 200', (done) => {
            request
            .get("/")
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect({
                error: false,
                data: 'server is working fine!'
            })
            .end(done);
        })
    });


    describe('GET /allJobs', () => {
        
        it('should get all jobs in response with status 200', (done) => {
            request
            .get("/allJobs")
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect( (response:any) => {
                expect(response.body.error).false;
                expect(response.body.data).exist;
            })
            .end(done);
        })
    });

    
    describe('GET /failedJobs', () => {
        
        it('should get all failed jobs in response with status 200', (done) => {
            request
            .get("/failedJobs")
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect( (response:any) => {
                expect(response.body.error).false;
                expect(response.body.data).exist;
            })
            .end(done);
        })
    });

    describe('GET /jobId/:jobId', () => {
        
        it('should retrieve the a job by it\'s \'jobId\' in response with status 200', (done) => {            
            request
            .post('/addOne')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .expect( (response:any) => {
                expect(response.body.error).false;
                jobId = (response.body.data.job_id).exist;
            })
            .then((res:any) => {
                jobId = res.body.data.job_id;
                request
                .get("/jobId/"+jobId)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .expect( (response:any) => {
                    expect(response.body.error).false;
                    expect(response.body.data).exist;
                    expect(response.body.data[0].job_id).equal(jobId);
                })
                .end(done);
            });
            
        });
    });


    describe('GET /randomGarbage', () => {
        
        it('should get response with status 404', (done) => {
            request
            .get("/randomGarbage")
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(404)
            .expect( (response:any) => {
                expect(response.body.error).true;
                expect(response.body.data).exist;
            })
            .end(done);
        })
    });


});
