# Testing-demo

Project for demonstration purposes of using `supertest`, `supertest-session` and using `nyc` to measure test coverage.

### Clone the repository

Execute the following in your `console` or `git-bash`:

`git clone https://gitlab.com/bitwidth/testing-demo.git`

### To get started

1. First install all the dependencies by using:

`npm i`

2. Change the respective values according to your postgres database in the `./config/postgres.json` file:

```json
{
    "databaseName": "yourDBname",
    "userName": "yourPostgresUsername",
    "password": "yourPasswordForTheUsername",
    "host": "localhost",
    "port": 5432
}
```

Preferably leave the `host` as it is, and just change the `port` if it is different.

3. Open the `Presentation.pdf` and follow along.