import { JobService } from '../services/jobs';

export class JobsAPI {

    static listAllJobs = (request:any, response:any) => {
        JobService.listAllJobs()
        .then((allJobs:any) => {
            response.status(200);
            response.send({
                error: false,
                data: allJobs
            });
        })
        .catch((error:any) => {
            response.status(500);
            response.send({
                error: true,
                data: "Please try again. Couldn't list all jobs."
            });
        });
    }

    
    static listAllFailedJobs = (request:any, response:any) => {
        JobService.listAllFailedJobs()
        .then((failedJobs:any) => {
            response.status(200);
            response.send({
                error: false,
                data: failedJobs
            });
        })
        .catch((error:any) => {
            response.status(500);
            response.send({
                error: true,
                data: "Please try again. Couldn't list all \'failed\' jobs."
            });
        });
    }


    static listOneJob = (request:any, response:any) => {
        
        const jobId = request.params.jobId;
        JobService.listOneJob(jobId)
        .then((oneJob:any) => {
            response.status(200);
            response.send({
                error: false,
                data: oneJob
            });
        })
        .catch((error:any) => {
            response.status(500);
            response.send({
                error: true,
                data: "Please try again. Couldn't list all \'failed\' jobs."
            });
        });
    }


    static addOneJob = (request:any, response:any) => {
        JobService.addOneJob()
        .then((addedJob:any) => {
            response.status(201);
            response.send({
                error: false,
                data: addedJob
            });
        })
        .catch((error:any) => {
            response.status(500);
            response.send({
                error: true,
                data: "Please try again. Couldn't add the job."+error
            });
        });
    }
}
