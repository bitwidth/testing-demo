import  *  as models from '../models/allModels';
const fs = require('fs');
const path = require("path");


const modelsObject:any = models;
const Sequelize = require('sequelize');
const postgresObj = JSON.parse(fs.readFileSync(path.resolve(__dirname,'../../../config/postgres.json'), 'utf-8'));

const sequelize = new Sequelize(postgresObj.databaseName, postgresObj.userName, postgresObj.password, {
    host: postgresObj.host,
    port: postgresObj.port,
    dialect: 'postgres',
    pool: {
        acquire: 20000, //in milliseconds
    },
    logging: false
});

/* or with an URI

const sequelize = new Sequelize('postgres://username:password@localhost:port/database_name',{
    options...
});

*/

sequelize.authenticate()
    .then( (result:any) => {
        // console.log("\nConnection established to database: "+sequelize.getDatabaseName());
    })
    .catch( (error:any) => {
        console.error('\nError - '+error);
        process.exit(1);
    });


Object.keys(models).forEach( (eachModel:any) =>{
    
    const model = modelsObject[eachModel](sequelize, Sequelize.DataTypes);
    
    model.sync()
    .then( (result:any) => {
        // console.log('\nSynchronized table: \''+result.getTableName()+'\' in database.');
    })
    .catch( (error:any) => {
        console.error('\nSomething went wrong while syncing :'+error);
    });
});

export { sequelize };