import { sequelize } from "./connection"
const { v4: uuidv4 } = require('uuid');

const statusDeveloper = ['dequeued'​, 'generated​', 'uploaded'​, 'notified'​, 'failed'];
const statusGeneral =   ['processing​', 'uploading​', 'completed​', 'failed​'];

export class ReportJobsDAO {

    private model = sequelize.model("report_job");

    listAll = () => {
        return this.model.findAll({
            where:{}
        });
    }

    listFailed = () => {
        return this.model.findAll({
            where:{
                status_developer: 'failed'
            }
        });
    }

    listOne = (jobId:any) => {
        return this.model.findAll({
            where:{
                job_id: jobId
            }
        });
    }

    addOne = () => {
        return this.model.create({
            job_id: uuidv4(),
            client_request_id: uuidv4(),
            report_request_id: uuidv4(),
            status_developer: statusDeveloper[Math.floor((Math.random() * 5))],
            status_general: statusGeneral[Math.floor((Math.random() * 4))],
            generated_timestamp: new Date().toUTCString()
        })
    }
}
