const statusDeveloper = ['dequeued'​, 'generated​', 'uploaded'​, 'storage', 'notified'​, 'failed'];
const statusGeneral =   ['processing​', 'uploading​', 'completed​', 'failed​'];

export class ReportJobsModel {

    constructor(sequelize:any, DataTypes:any) {
        
        const reportJobs = sequelize.define('report_job', {

        job_id: {type:DataTypes.UUID, primaryKey: true},
        client_request_id: {type:DataTypes.UUID, allowNull:false},
        report_request_id: {type:DataTypes.UUID, allowNull:false},
        status_developer: {type:DataTypes.ENUM, allowNull:false, values:statusDeveloper, defaultValue: statusDeveloper[0]},
        status_general: {type:DataTypes.ENUM, allowNull:false, values:statusGeneral, defaultValue: statusGeneral[0]},
        generated_timestamp: {type:DataTypes.DATE, allowNull:false}
        
        });
        
        return reportJobs;
    }
    // checkout associations and hooks
}
