const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const { v4: uuidv4 } = require('uuid');

const router = express.Router();

import { JobsAPI } from './routes/jobs';

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

router.get('/', (request:any, response:any) => {
    response.status(200);
    response.cookie('session_id', uuidv4());
    response.json({
        error: false,
        data: "server is working fine!"
    });
});

router.get("/jobId/:jobId", JobsAPI.listOneJob);

router.get("/allJobs", JobsAPI.listAllJobs);

router.get("/failedJobs", JobsAPI.listAllFailedJobs);

router.post("/addOne", JobsAPI.addOneJob);

router.get("/*", (request:any, response:any) => {
    response.status(404);
    response.json({
        error: true,
        data: "The requested route doesn't exist. :("
    });
});

app.use('/', router);

export { app };
