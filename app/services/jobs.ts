import { ReportJobsDAO } from '../daos/report_jobs';

const reportJobsDAO = new ReportJobsDAO();

export class JobService {

    static listAllJobs = () => {

        return new Promise ((resolve:any, reject:any) => {
            reportJobsDAO.listAll()
            .then( (allJobs:any) => {
                // console.log('Success. Listed all the jobs.');
                resolve(allJobs);
            })
            .catch( (error:any) => {
                console.log('Error: ', error);
                reject(error);
            });
        });
    }
    
    static listAllFailedJobs = ()=> {

        return new Promise ((resolve:any, reject:any) => {
            reportJobsDAO.listFailed()
            .then( (failedJobs:any) => {
                // console.log('Success. Listed all the failed jobs.');
                resolve(failedJobs);
            })
            .catch( (error:any) => {
                console.log('Error: ', error);
                reject(error);
            });
        });
    }

    static listOneJob = (jobId:any) => {
        return new Promise((resolve:any, reject:any) => {
            reportJobsDAO.listOne(jobId)
            .then( (listJob:any) =>{
                // console.log('Success. Listed one job with \'job_id\' = '+jobId);
                resolve(listJob);
            })
            .catch( (error:any) => {
                console.log('Error: ', error);
                reject(error);
            });
        });
    }

    static addOneJob = ()=> {
        return new Promise((resolve:any, reject:any) => {
            reportJobsDAO.addOne()
            .then( (addedJob:any) =>{
                // console.log('Success. Added one job.');
                resolve(addedJob);
            })
            .catch( (error:any) => {
                console.log('Error: ', error);
                reject(error);
            });
        });
    }
}
